package com.tw.pathashala;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static com.tw.pathashala.Rectangle.createSquare;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

class RectangleTest {

    private static Rectangle rectangle1x1;
    private static Rectangle rectangle2x1;

    @BeforeAll
    static void SetUp() {
        rectangle1x1 = new Rectangle(1, 1);
        rectangle2x1 = new Rectangle(2, 1);
    }

    @Test
    void should_have_an_area_of_one_if_length_one_and_breadth_one() {
        assertThat(rectangle1x1.area(), is(equalTo(1)));
    }

    @Test
    void should_have_an_area_of_two_if_length_two_and_breadth_one() {
        assertThat(rectangle2x1.area(), is(equalTo(2)));
    }

    @Test
    void should_have_a_perimeter_of_four_if_length_one_and_breadth_one() {
        assertThat(rectangle1x1.perimeter(), is(equalTo(4)));
    }

    @Test
    void should_have_a_perimeter_of_six_if_length_two_and_breadth_one() {
        assertThat(rectangle2x1.perimeter(), is(equalTo(6)));
    }

    @Nested
    static class Square {
        private static Rectangle square2;

        @BeforeAll
        static void SetUp() {
            square2 = createSquare(2);
        }

        @Test
        void should_calculate_area_of_a_square() {
            assertThat(square2.area(), is(equalTo(4)));
        }

        @Test
        void should_calculate_perimeter_of_a_square() {
            assertThat(square2.perimeter(), is(equalTo(8)));
        }
    }
}
