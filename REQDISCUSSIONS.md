## Requirement 1 discussion

* Talk about packaging code and the benefits of name-spacing

* Allow the trainees to get comfortable writing the test file first, even without source files. Talk about ``Option + Enter``
and how it allows the developer generate the source class, constructors, methods and parameters

* Discuss how writing the test first allows the developer to focus on the contract first. This helps them come up with
an elegant contract without worrying about implementation

* Drive the class towards writing incremental failing tests with minimal test data, which should be reflective of the
functionality being tested. Eg:``Rectangle(1, 1)`` instead of ``Rectangle(4,5)``

* You can switch between test and source with ``Cmd + Shift + T``

* Some approaches might include a procedural approach having an area calculator or an already over-engineered approach
(__YAGNI__ can be mentioned here) including a premature class hierarchy

* Discuss about the method name ``area()``. Common alternatives given by trainees are ``getArea()`` and ``calculateArea()``. This
should be an initial discussion of how naming also ties in with encapsulation by hiding implementation
    * Talk about __how__ the area is returned isn't relevant, as it could be fetched directly from a store, or calculated on
    the fly. What's relevant is that the method name should be indicative of the value it evaluates to in an expression

---
## Requirement 2 discussion

* This is a re-enforcement requirement. Helps the trainees apply learning from the discussion of the last requirement

* There can always be new avenues in discussion based off the showcase

---
## Requirement 3 discussion

* Normally we've noticed three approaches to this requirement (Drive these three approaches by asking pros and cons):

    * __The naive approach of creating a new square class:__ This although has better intent revealing (__Principle #2__)
    it will suffer from not having fewer elements (__Principle #4__) and duplicated code (__Principle #3__)

    * __Inherit square class from rectangle by reusing base constructor and method:__ This works well on the readability
    front (__Principle #2__) and also doesn't duplicate behavior (__Principal #3__), however, it doesn't have fewer
    elements (__Principle #4__), and more importantly this will result in a class without behavior. Have a discussion
    and ask the class if it's alright to have a class without behavior

    * __Overload constructor in Rectangle class:__ Do showcase this approach to discuss constructor chaining. Although
    the approach doesn't duplicate code (__Principle #3__) and has fewer elements (__Principle #4__), the statement
    ``new Rectangle(1)`` constructing a square doesn't reveal intention (__Principle #2__) and is confusing to the
    client

* We can talk about __DRY__ and __KISS__ as part of these principles

* There might be questions around the static method name ``createSquare()`` instead of ``square()``. If this isn't asked
by the trainees, we would need to ask this question. We name it with the __create prefix__ to indicate to the client
that this method is indeed a factory and creates a new instance on the heap. That is information we need to be explicit
with the client about

* Talk about test classes nesting in Junit 5. Derive meaningful segregation, from the class, of test cases based on this
feature

* Discuss about static imports introduced in the final approach.
