## Rectangle Exercise

The primary objective of the exercise is to enforce the four rules of simple design as mentioned in this article - 
https://martinfowler.com/bliki/BeckDesignRules.html

Being one of the first few exercises which will be run, there will remain focus on naming, formatting, commit messages
 and relevance of the test cases and whether they are being written incrementally.

---

The requirements will be delivered in 3 increments as follows:

### Requirement 1

As a fan of geometry, I want to know the area of rectangle (Git tag: requirement-1)

### Requirement 2

As a fan of geometry, I want to know the perimeter of a rectangle (Git tag: requirement-2)

### Requirement 3

As a fan of geometry, I want to know the area and perimeter of a square (Git tag: requirement-3)

---

The tags associated with each requirement will reflect the expected code at the end of each requirement. Having said that,
 we always welcome meaningful (or sometimes even better) variations from the trainees.

Each requirement tag will have release notes in __REQDISCUSSIONS.md__ as well as written in remote (Gitlab). The release
notes have information regarding the often observed discussions and learning from each of the requirement.
